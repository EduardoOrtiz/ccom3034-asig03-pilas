#include <iostream>
#include <cassert>
#include "superSt.h"

using namespace std;

void testPostfix();
void testInfix();

int main() {
	int a = 0;

	testPostfix();
	testInfix();

	SuperString s2 = static_cast<string>("5 5 * 10 7 + + ");
	if ( s2.evalPostfix(a)) {
		cout << "a: " << a << endl;
	}
	
	s2 = (string)"( 42 * 42 / 42 ) + ( 42 - 42 + 42 - 42 )";
	if ( s2.evalInfix(a)) {
		cout << "a: " << a << endl;
	}
	return 0;
}

void testPostfix()
{
	int a = (3 * 5) + 9 / (3 * 3);
	int b;

	SuperString Test1 = (string)("3 3 * 9 / 3 5 * +");

	Test1.evalPostfix(b);

	assert(a == b);

	int c = 0, d = 0;

	SuperString Test2 = (string)("1 2 3 4 5 +");
	SuperString Test3 = (string)("+ + 11 22 33");

	Test2.evalPostfix(d);

	assert(c == d);

	Test3.evalPostfix(d);

	assert(c == d);
}

void testInfix()
{
    int res;
    SuperString st = (string)"( 9 + 5 ) / ( 10 - 3 )";

    assert(st.evalInfix(res));
    assert(res == 2);

    st = (string)"( 9 + 5 * 1 ) * ( 1 - 2 )";
    assert(st.evalInfix(res));
    assert(res == -14);

    st = (string)"( 9 + 5 * 1 ))";
    // this evalInfix should be false because of
    // parenthesis unbalance
    assert(st.evalInfix(res)==false);

	st = (string)"( 3 - 5 * 4 ) - ( 5 / 3 - 2 * 4 )";
	
	assert(st.evalInfix(res));
	assert(res == -10);
}
