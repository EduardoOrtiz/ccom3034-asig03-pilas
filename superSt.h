#ifndef SUPERST_H
#define SUPERST_H

#include <iostream>
#include <stack>
#include <vector>
using namespace std;


class SuperString : public string {
public:
	SuperString()  {}
	SuperString(string st) : string(st) {}
	SuperString(int a);
	
	vector<SuperString> tokenize(const string & delim);
	SuperString& operator= (string s);
	SuperString& operator= (int n);
	bool toInt(int& i);
	bool evalPostfix(int &result);
	bool evalInfix(int &);

};


#endif

