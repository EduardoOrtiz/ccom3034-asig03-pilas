/*=============================================================================

=============================================================================*/
#include "superSt.h"
#include <vector>
#include <cstdlib>
#include <sstream>
using namespace std;

// Tokenizes (splits) the SuperString, based on the delimiter
// Parameters: delim: the delimiter
// Returns: a vector of SuperStrings that were split

vector<SuperString> SuperString::tokenize(const string & delim) {
	vector<SuperString> tokens;
	size_t p0 = 0, p1 = string::npos;
	while(p0 != string::npos) {
    	p1 = find_first_of(delim, p0);
    	if(p1 != p0) {
      		string token = substr(p0, p1 - p0);
      		tokens.push_back(token);
      	}
		p0 = find_first_not_of(delim, p1);
	}
	return tokens;
}

// Overload of the operator= for using stataments such as:
// SuperString st = static_cast<string>("3 4 5 + * 1 +");
SuperString& SuperString::operator= (string s) {
	this->assign(s);
	return *this;
}


// Overload of the operator= for using stataments such as:
// SuperString st = 209;
SuperString& SuperString::operator= (int n) {
	this->assign(dynamic_cast< std::ostringstream & >(( std::ostringstream()
   		<< std::dec << n ) ).str());
	return *this;
}

// Overload of the constructor, allows statements such as:
// SuperString st(254);
SuperString::SuperString(int a) {
   this->assign(dynamic_cast< std::ostringstream & >(( std::ostringstream()
   		<< std::dec << a ) ).str());
}

// Converts the SuperString to int, if possible
bool SuperString::toInt(int& i) {
   char c = this->c_str()[0];
   if(empty() || ((!isdigit(c) && (c != '-') && (c != '+')))) return false ;

   char * p ;
   i = strtol(c_str(), &p, 10) ;

   return (*p == 0) ;
}


// Evaluates the postfix expression.
// Returns:
//   - result: integer through which we return the result of the eval
//   - boolean return value: true if the expression is valid
bool SuperString::evalPostfix(int &result) {

	stack<int> PostStack;

	int operand1, operand2, dummy;

    unsigned int counter = 0;

	vector<SuperString> Expression = this->tokenize(" ");

	if(Expression.size() < 3) return false;

	while(counter != Expression.size())
	{
		if(Expression[counter].toInt(dummy))
			PostStack.push(dummy);

		else if(Expression[counter] == "+")
		{
			if(PostStack.empty()) return false;

			operand1 = PostStack.top();
			PostStack.pop();

			if(PostStack.empty()) return false;

			operand2 = PostStack.top();
			PostStack.pop();

			PostStack.push(operand1 + operand2);
		} else if(Expression[counter] == "-")
		{
			if(PostStack.empty()) return false;

			operand1 = PostStack.top();
			PostStack.pop();

			if(PostStack.empty()) return false;

			operand2 = PostStack.top();
			PostStack.pop();

			PostStack.push(operand2 - operand1);
		} else if(Expression[counter] == "*")
		{
			if(PostStack.empty()) return false;

			operand1 = PostStack.top();
			PostStack.pop();

			if(PostStack.empty()) return false;

			operand2 = PostStack.top();
			PostStack.pop();

			PostStack.push(operand1 * operand2);
		} else if(Expression[counter] == "/")
		{
			if(PostStack.empty()) return false;

			operand1 = PostStack.top();
			PostStack.pop();

			if(PostStack.empty()) return false;

			operand2 = PostStack.top();
			PostStack.pop();

			if(operand1 == 0) return false;

			PostStack.push(operand2 / operand1);
		} else return false;

		counter++;
	}

	dummy = PostStack.top();
	PostStack.pop();

	if(!PostStack.empty()) return false;

	result = dummy;

	return true;
}

//Evaluates the infix expression
//Order of operations will be subtraction, addition, division,
//and multiplication which will be denoted as 1, 2, 3, and 4 respectively
//0 will denote (
//Will change to postfix using another vector and then evaluate
bool SuperString::evalInfix(int& result)
{
	//this doesn't look elegant at all
	stack<short> Parallels;
    stack<SuperString> Operators;
	int dummy;
    unsigned int counter = 0, pivot = 0;
	bool found_right = false;

	vector<SuperString> inExp = this->tokenize(" ");
	vector<SuperString> outExp;
	SuperString result_str;

	if(inExp.size() < 3) return false;

	while(counter != inExp.size())
	{
		if(inExp[counter].toInt(dummy))
			outExp.push_back(inExp[counter] + " ");

		if(inExp[counter] == ")") return false;

		if(inExp[counter] == "(")
		{
			Operators.push(inExp[counter]);
			Parallels.push(0);
			//now to deal with the pivot
			while(pivot != inExp.size() and !found_right)
			{
				pivot++;
				
				if(pivot == inExp.size()) return false;

				if(inExp[pivot].toInt(dummy))
					outExp.push_back(inExp[pivot] + " ");

				else if(inExp[pivot] == "*")
				{
					Parallels.push(4);
					Operators.push(inExp[pivot]);
				}
				else if(inExp[pivot] == "/")
				{
					if(Parallels.empty())
					{
						Parallels.push(3);
						Operators.push(inExp[pivot]);
					}
					else if(Parallels.top() > 3)
					{
						while(Parallels.top() > 3)
						{
							outExp.push_back(Operators.top() + " ");
							Operators.pop();
							Parallels.pop();
							if(Parallels.empty()) break;
						}
						Operators.push(inExp[pivot]);
						Parallels.push(3);
					}
					else
					{
						Operators.push(inExp[pivot]);
						Parallels.push(3);
					}
				}
				else if(inExp[pivot] == "+")
				{
					if(Parallels.empty())
					{
						Parallels.push(2);
						Operators.push(inExp[pivot]);
					}
					else if(Parallels.top() > 2)
					{
						while(Parallels.top() > 2)
						{
							outExp.push_back(Operators.top() + " ");
							Operators.pop();
							Parallels.pop();
							if(Parallels.empty()) break;
						}
						Operators.push(inExp[pivot]);
						Parallels.push(2);
					}
					else
					{
						Operators.push(inExp[pivot]);
						Parallels.push(2);
					}
				}
				else if(inExp[pivot] == "-")
				{
					if(Parallels.empty())
					{
						Parallels.push(1);
						Operators.push(inExp[pivot]);
					}
					else if(Parallels.top() > 1)
					{
						while(Parallels.top() > 1)
						{
							outExp.push_back(Operators.top() + " ");
							Operators.pop();
							Parallels.pop();
							if(Parallels.empty()) break;
						}
						Operators.push(inExp[pivot]);
						Parallels.push(1);
					}
					else
					{
						Operators.push(inExp[pivot]);
						Parallels.push(1);
					}
				}
				else if(inExp[pivot] == ")")
				{
					found_right = true;
					pivot++;
					while(Parallels.top() != 0)
					{
						outExp.push_back(Operators.top() + " ");
						Operators.pop();
						Parallels.pop();
					}
					
					Operators.pop();
					Parallels.pop();
					
				}
				else return false;

				counter = pivot;
			}
			
			
		}
		
		if(counter >= inExp.size()) break;
		
		if(inExp[counter] == "*")
		{
			Parallels.push(4);
			Operators.push(inExp[counter]);
		}
		else if(inExp[counter] == "/")
		{
			if(Parallels.empty())
			{
				Parallels.push(3);
				Operators.push(inExp[counter]);
			}
			else if(Parallels.top() > 3)
			{
				while(Parallels.top() > 3)
				{
					outExp.push_back(Operators.top() + " ");
					Operators.pop();
					Parallels.pop();
					if(Parallels.empty()) break;
				}
				Operators.push(inExp[counter]);
				Parallels.push(3);
			}
			else
			{
				Operators.push(inExp[counter]);
				Parallels.push(3);
			}
		}
		else if(inExp[counter] == "+")
		{
			if(Parallels.empty())
			{
				Parallels.push(2);
				Operators.push(inExp[counter]);
			}
			else if(Parallels.top() > 2)
			{
				while(Parallels.top() > 2)
				{
					outExp.push_back(Operators.top() + " ");
					Operators.pop();
					Parallels.pop();
					if(Parallels.empty()) break;
				}
				Operators.push(inExp[counter]);
				Parallels.push(2);
			}
			else
			{
				Operators.push(inExp[counter]);
				Parallels.push(2);
			}
		}
		else if(inExp[counter] == "-")
		{
			if(Parallels.empty())
			{
				Parallels.push(1);
				Operators.push(inExp[counter]);
			}
			else if(Parallels.top() > 1)
			{
				while(Parallels.top() > 1)
				{
					outExp.push_back(Operators.top() + " ");
					Operators.pop();
					Parallels.pop();
					if(Parallels.empty()) break;
				}
				Operators.push(inExp[counter]);
				Parallels.push(1);
			}
			else
			{
				Operators.push(inExp[counter]);
				Parallels.push(1);
			}
		} else return false;

		counter++;
		pivot = counter;
		found_right = false;
	}
	
	while(!Operators.empty())
	{
		outExp.push_back(Operators.top() + " ");
		Operators.pop();
		Parallels.pop();
	}
	
	for(int c = 0; c < outExp.size(); c++)
		result_str = result_str + outExp[c];

	//cout << result_str << endl;
	
	if(result_str.evalPostfix(result))
		return true;

	return false;
}
